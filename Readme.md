# Prerequisites

Clone this repository to your local machine.

```bash
git clone https://gitlab.com/ytannus/pyms-boilerplate
cd pyms-boilerplate
```

## Create a virtual environment

Use `venv` or `conda` to create a virtual environment.

Using venv
```bash
python3 -m venv pyms-boilerplate
source pyms-boilerplate/bin/activate
```

Using conda
```bash
conda create -n pyms-boilerplate python=3.7
conda activate pyms-boilerplate
```

## Install dependencies
```
pip install -r requirements.txt
```

# Create a new project

```bash
pyms startproject
```

```bash

You've downloaded /home/balu/.cookiecutters/cookiecutter-pyms before. Is it okay to delete and re-download it? [yes]: yes
1. project_repo_url [https://github.com/python-microservices/microservices-scaffold]: https://github.com/JinnaBalu/sample-pyms    
2. project_name [Python Microservices Boilerplate]: Python Microservice
3. project_folder [python_microservice]: 
4. project_short_description [Python Boilerplate contains all the boilerplate you need to create a Python package.]: Python Microservice Sample with PyMS
5. create_model_class [y]: y
6. microservice_with_swagger_and_connexion [y]: y
7. microservice_with_traces [y]: y
8. microservice_with_metrics [y]: n
9. application_root [/python_microservice]:  /python_microservice
10. Select open_source_license:
1 - MIT license
2 - BSD license
3 - ISC license
4 - Apache Software License 2.0
5 - GNU General Public License v3
6 - Not open source
Choose from 1, 2, 3, 4, 5, 6 [1]: 6
```

```bash
cd <project_folder>
pip install -r requirements.txt
```

Edit `manage.py`

```python
# encoding: utf-8
from flask_script import Manager
from flask import jsonify, current_app, request
from project.app import create_app

app = create_app()
manager = Manager(app)

@app.route("/")
def index():
    app.logger.info("Headers: \n{}".format(request.headers))
    return jsonify({"main": "Hi There {}".format(current_app.config["APP_NAME"])})

if __name__ == '__main__':
  manager.run()
```

# Run

```bash
python manage.py runserver
```

### Reference
https://jinnabalu.medium.com/create-the-python-microservice-with-pyms-d46248187d74
https://github.com/python-microservices/microservices-scaffold/

# Docker

Consider to use the following Dockerfile:

```
FROM python:3.6.4-alpine3.7

RUN apk add --update curl gcc g++ git libffi-dev openssl-dev python3-dev build-base linux-headers py3-pip \
    && rm -rf /var/cache/apk/*
RUN ln -s /usr/include/locale.h /usr/include/xlocale.h

ENV PYTHONUNBUFFERED=1 ENVIRONMENT=pre APP_HOME=/microservice/
ENV DATABASE_DIR=database
ENV PYMS_CONFIGMAP_FILE="$APP_HOME"config-docker.yml
RUN mkdir $APP_HOME && adduser -S -D -H python

RUN chown -R python $APP_HOME
WORKDIR $APP_HOME
RUN pip install gevent==1.2.2 gunicorn==19.7.1

ADD . $APP_HOME
RUN pip install --upgrade pip
RUN pip install --upgrade setuptools
RUN pip install swagger-spec-validator openapi_spec_validator werkzeug==2.0.3 psycopg2-binary==2.9.3
RUN pip install -r requirements.txt

RUN sed -i 's/from flask._compat import text_type/from ._compat import text_type/' /usr/local/lib/python3.6/site-packages/flask_script/__init__.py

RUN mkdir $DATABASE_DIR
RUN chmod 777 $DATABASE_DIR

EXPOSE 5000
USER python

CMD ["gunicorn", "--workers", "8", "--log-level", "INFO", "--bind", "0.0.0.0:5000", "manage:app"]
```