# encoding: utf-8
from flask_script import Manager
from flask import jsonify, current_app, request
from project.app import create_app

app = create_app()
manager = Manager(app)

@app.route("/")
def index():
    app.logger.info("Headers: \n{}".format(request.headers))
    return jsonify({"main": "Hi There {}".format(current_app.config["APP_NAME"])})

if __name__ == '__main__':
  manager.run()