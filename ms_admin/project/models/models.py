# encoding: utf-8
from __future__ import absolute_import, print_function, unicode_literals
import datetime
from email.policy import default
from sqlalchemy import Column, Boolean, String, DateTime, Date, ForeignKey, Integer
from project.models.init_db import db

from sqlalchemy.dialects.postgresql import UUID
import uuid


class Customer(db.Model):
    """Customer Model"""
    __tablename__ = 'customer'
    __table_args__ = {'schema': 'master'}

    id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
    name = Column(String, nullable=False)
    clean_name = Column(String, nullable=False)
    active = Column(Boolean, default=False, nullable=False)
    deleted = Column(Boolean, default=False, nullable=False)
   

# class User(db.Model):
#     __tablename__ = 'user'
#     __table_args__ = {'schema': customer.clean_name}

#     id = Column(UUID(as_uuid=True), primary_key=True, default=uuid.uuid4, unique=True, nullable=False)
#     name = Column(String, nullable=False)
#     last_name = Column(String, nullable=False)
#     email = Column(String, nullable=False)
#     phone = Column(String, nullable=False)
#     password = Column(String, nullable=False)
#     language = Column(String, nullable=False)
#     active = Column(Boolean, default=True, nullable=False)
#     deleted = Column(Boolean, default=False, nullable=False)
    
#     # films = db.relationship("Customer", secondary="film_cast")
