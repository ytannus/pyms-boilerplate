# encoding: utf-8
from __future__ import absolute_import, print_function, unicode_literals
from typing import Tuple

import connexion
from flask import jsonify

from project.models.init_db import db
from project.models.models import Customer
from project.serializers.serializers import CustomerSchema


def get() -> Tuple[dict, int]:
    query = Customer.query.paginate(
        connexion.request.args.get("paginationKey", 1),
        connexion.request.args.get("pageSize", 5)
    )
    schema = CustomerSchema()
    result = schema.dump(query.items, many=True)
    return jsonify(result), 200


def search() -> Tuple[dict, int]:
    return get()

def post() -> dict:
    if connexion.request.is_json:
        data = connexion.request.get_json()
        customer = Customer(name=data["name"], clean_name=data["clean_name"], active=data["active"], deleted=data["deleted"])
        db.session.add(customer)
        db.session.commit()

        return jsonify(CustomerSchema().dump(customer))
    return jsonify({})