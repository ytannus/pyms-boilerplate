from marshmallow import fields
from marshmallow_sqlalchemy import SQLAlchemySchema

from project.models.models import Customer


class CustomerSchema(SQLAlchemySchema):
    class Meta:
        model = Customer
        fields = ('id', 'name', 'clean_name', 'active', 'deleted')

